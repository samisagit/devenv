syntax enable

" Flash screen instead of beep sound
set visualbell

" Change how vim represents characters on the screen
set encoding=utf-8

" Set the encoding of files written
set fileencoding=utf-8

" Set Go styling
autocmd Filetype go setlocal tabstop=4 shiftwidth=4 softtabstop=4

" Set generic width
set shiftwidth=4

" Maintain undo history between sessions
set undofile
set undodir=~/.vim/undodir

filetype plugin indent on

" Give backspace liberties
set backspace=indent,eol,start

" Go set up
let g:go_fmt_command = "goimports"
au filetype go inoremap <buffer> . .<C-x><C-o>
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_extra_types = 1

" Status line types/signatures.
let g:go_auto_type_info = 1

" Key bindings
:nmap , :

FROM debian

# Install global dependencies
RUN apt-get update -y && apt-get upgrade -y && apt-get install -y \
        sudo \
        build-essential \
        libncurses-dev \
        wget \
        curl \
        zsh \
        git \
        tmux

RUN git config --global user.name "samisagit"
RUN git config --global user.email "sam@whiteteam.co.uk"

# Set up sudo user
RUN adduser --disabled-password --gecos '' sam
RUN adduser sam sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER sam

# Set up oh-my-zsh
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
RUN echo "set-option -g default-shell /usr/bin/zsh" >> ~/.tmux.conf

WORKDIR /home/sam

# Install Go
RUN wget -q https://dl.google.com/go/go1.14.linux-amd64.tar.gz && sudo tar -C /usr/local -xzf go1.14.linux-amd64.tar.gz
RUN echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.zshrc
RUN rm go1.14.linux-amd64.tar.gz

# Install VIM
RUN git clone https://github.com/vim/vim.git
RUN cd vim/src && make && sudo make install
RUN rm -rf /home/sam/vim
RUN touch ~/.vimrc

# Set up VIM plugins
RUN git clone https://github.com/fatih/vim-go.git ~/.vim/pack/plugins/start/vim-go
RUN export PATH=$PATH:/usr/local/go/bin && vim -e -T dumb -n -c "GoInstallBinaries" -c "q!"

# Configure VIM
#RUN mkdir -p ~/.vim/colors \
#	&& cd ~/.vim/colors \
#	&& curl -O https://raw.githubusercontent.com/nanotech/jellybeans.vim/master/colors/jellybeans.vim

CMD ["usr/bin/zsh"]
